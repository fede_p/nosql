package persistencia;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import org.apache.commons.validator.routines.EmailValidator;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import static com.mongodb.client.model.Filters.*;

import java.util.ArrayList;
import java.util.List;

public class ManagerConexion {

	private MongoDatabase database = null;

	private String connectionString = "mongodb://localhost:27017";

	private String nombreBaseDeDatos = "BaseNoSQL1";
	private String nombreEsquemaCorreos = "esquemaCorreos";
	private String nombreEsquemaComentarios = "esquemaComentarios";

	public ManagerConexion() {
		if (database == null) {
			try {
				MongoClient mongoClient = new MongoClient(new MongoClientURI(connectionString));
				database = mongoClient.getDatabase(nombreBaseDeDatos);
			} catch (Exception e) {
				System.out.println("ERROR ::::" + e.getMessage());
			}
		}
	}

	// HELPERS - OPERACIONES PRIVADAS
	private boolean correoExists(String correo) {
		MongoCollection<Document> collection = database.getCollection(nombreEsquemaCorreos);
		Document myDoc = collection.find(new BasicDBObject("_id", correo)).first();
		return myDoc != null;
	}

	private Document buscarComentario(String idComentario) {
		MongoCollection<Document> collection = database.getCollection(nombreEsquemaComentarios);
		Document myDoc = collection.find(eq("_id", new ObjectId(idComentario))).first();
		return myDoc;
	}

	////////////////////////////////////////////////////////////////////
	/*
	 * 1)Crear Usuario - Se pasar� el correo electr�nico del usuario a crear, este
	 * correo debe ser �nico en el sistema
	 */
	public void crearUsuario(String correo) throws Exception {
		if (!EmailValidator.getInstance().isValid(correo))
			throw new Exception("El correo no tiene el formato correcto");

		if (correoExists(correo))
			throw new Exception("El correo ya existe");

		Document doc = new Document("_id", correo);
		MongoCollection<Document> collection = database.getCollection(nombreEsquemaCorreos);
		collection.insertOne(doc);
	}

	/*
	 * 2)Crear Comentario - Se pasar� el correo electr�nico de un usuario del
	 * sistema, y el texto de los comentarios (menor a 256 caracteres).El sistema le
	 * asignar� un identificador de forma autom�tica al comentario creado.
	 */
	public String crearComentario(String correo, String comentario) throws Exception {
		if (!correoExists(correo))
			throw new Exception("El correo no existe");

		Document doc = new Document("comentario", comentario).append("correo", correo).append("listaComentarios", new ArrayList<>());
		MongoCollection<Document> collection = database.getCollection(nombreEsquemaComentarios);
		collection.insertOne(doc);
		ObjectId id = (ObjectId) doc.get("_id");
		return id.toString();
	}

	/*
	 * 3)Listar Comentarios de Usuario - Se pasar� el correo electr�nico de un
	 * usuario del sistema, y se retorna una lista con el identificador y contenido
	 * de todos los comentarios realizados por el usuario.
	 */
	public String listarComentarios(String correo) {
		MongoCollection<Document> collection = database.getCollection(nombreEsquemaComentarios);
		FindIterable<Document> myDoc = collection.find(eq("correo", correo));

		String retorno = "";
		for (Document i : myDoc) {
			retorno += i.toJson();
		}
		return retorno;
	}

	/*
	 * 4)Comentar Comentario - Se pasar� el identificador de un comentario existente
	 * en el sistema, un correo electr�nico que identifica al usuario que realizar�
	 * el nuevo comentario, y un texto con el nuevo comentario.
	 */
	public String comentarComentario(String idComentario, String correo, String textoComentario) throws Exception {

		String idComentarioNuevo = crearComentario(correo, textoComentario);

		MongoCollection<Document> collection = database.getCollection(nombreEsquemaComentarios);
		Document myDoc = collection.find(eq("_id", new ObjectId(idComentario))).first();
		Object listaComentarios = myDoc.get("listaComentarios");

		List<String> lista = (ArrayList<String>) listaComentarios;
		lista.add(idComentarioNuevo);
		myDoc.replace("listaComentarios", lista);

		collection.updateOne(eq("_id", new ObjectId(idComentario)), new Document("$set", myDoc));

		return idComentarioNuevo;
	}

	/*
	 * 5)Leer Comentario - Se pasar� el identificador de un comentario existente en
	 * el sistema, y se retorna el identificador del comentario, el texto que lo
	 * contiene, y una lista con todos los comentarios realizados sobre el. (Solo
	 * los de primer nivel, es decir si hay un comentario sobre el texto pasado con
	 * el identificador, y sobre este comentario hay m�s comentarios, estos
	 * comentarios no son considerados como comentarios del comentario inicial.)
	 */
	public String leerComentario(String idComentario) {
		Document doc = buscarComentario(idComentario);

		Object listaComentarios = doc.get("listaComentarios");
		List<String> lista = (ArrayList<String>) listaComentarios;

		String retorno = doc.toJson();
		for (String i : lista) {
			retorno = retorno.replace(i, buscarComentario(i).toJson());
		}

		return retorno;
	}

}
