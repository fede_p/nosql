package rest;

import java.util.HashMap;
import java.util.Map;
import org.jose4j.json.internal.json_simple.JSONObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import persistencia.ManagerConexion;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Servicio {

	protected static Logger logger = LoggerFactory.getLogger(Servicio.class);

	private ManagerConexion mongo = new ManagerConexion();

	@GET
	@Path("crearUsuario")
	public Response crearUsuario(@QueryParam("correo") String correo) {
		logger.info("altaCorreo " + correo);

		try {
			mongo.crearUsuario(correo);
			Map<String, Object> map = new HashMap<>();
			map.put("status", Response.Status.OK);
			map.put("message", "Se creó el usuario");
			JSONObject response = new JSONObject(map);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(createJsonError(Response.Status.BAD_REQUEST.getStatusCode(), e)).build();
		}
	}

	@GET
	@Path("altaComentario")
	public Response altaComentario(@QueryParam("correo") String correo, @QueryParam("comentario") String comentario) {
		logger.info("altaComentario de " + correo + ", comentario: " + comentario);

		try {
			String idComentario = mongo.crearComentario(correo, comentario);
			Map<String, Object> map = new HashMap<>();
			map.put("status", Response.Status.OK);
			map.put("message", "Se dio de alta ciomentario " + idComentario);
			JSONObject response = new JSONObject(map);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(createJsonError(Response.Status.BAD_REQUEST.getStatusCode(), e)).build();
		}
	}

	@GET
	@Path("comentarComentario")
	public Response comentarComentario(@QueryParam("idComentario") String idComentario, @QueryParam("correo") String correo, @QueryParam("comentario") String comentario) {
		logger.info("listarComentarios de " + correo);

		try {
			String idComentarioNuevo = mongo.comentarComentario(idComentario, correo, comentario);
			Map<String, Object> map = new HashMap<>();
			map.put("status", Response.Status.OK);
			map.put("message", "Se creó el comentario con id " + idComentarioNuevo + " para el comentario " + idComentario);
			JSONObject response = new JSONObject(map);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(createJsonError(Response.Status.BAD_REQUEST.getStatusCode(), e)).build();
		}
	}

	@GET
	@Path("leerComentario")
	public Response leerComentario(@QueryParam("idComentario") String idComentario) {
		logger.info("leer comentario " + idComentario);

		try {
			Object o = mongo.leerComentario(idComentario);
			Map<String, Object> map = new HashMap<>();
			map.put("status", Response.Status.OK);
			map.put("message", o);
			JSONObject response = new JSONObject(map);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(createJsonError(Response.Status.BAD_REQUEST.getStatusCode(), e)).build();
		}
	}

	@GET
	@Path("listarComentarios")
	public Response listarComentarios(@QueryParam("correo") String correo) {
		logger.info("listarComentarios de " + correo);

		try {
			Object o = mongo.listarComentarios(correo);
			Map<String, Object> map = new HashMap<>();
			map.put("status", Response.Status.OK);
			map.put("message", o);
			JSONObject response = new JSONObject(map);
			return Response.ok(response, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(createJsonError(Response.Status.BAD_REQUEST.getStatusCode(), e)).build();
		}
	}

	private static JSONObject createJsonError(int errorCode, Exception e) {
		Map<String, Object> map = new HashMap<>();
		map.put("errorCode", errorCode);
		map.put("errorMessage", e.getMessage());
		return new JSONObject(map);
	}

}
